import gitlab
from datetime import datetime, timedelta

# Replace with your GitLab instance URL and private token
gl = gitlab.Gitlab("https://gitlab.example.com", private_token="your_private_token")

# Replace with your project ID
project_id = "your_project_id"
project = gl.projects.get(project_id)

# Get deployments for the project
deployments = project.deployments.list()

# Calculate deployment frequency
time_period = timedelta(days=30)  # Change this value to set the desired time period
start_date = datetime.now() - time_period
deployment_count = 0

for deployment in deployments:
    deployment_date = datetime.strptime(deployment.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    if deployment_date >= start_date:
        deployment_count += 1

deployment_frequency = deployment_count / time_period.days
print(f"Deployment frequency (deployments per day) in the last {time_period.days} days: {deployment_frequency}")